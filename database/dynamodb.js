'use strict';

const AWS = require('aws-sdk');

const options = {
    region: 'us-west-2',
};

const localDebug = true;

// connect to local DB if running offline
if (localDebug || process.env.IS_OFFLINE) {
    options.region = 'localhost';
    options.endpoint = 'http://localhost:8000';
}

const documentClient = new AWS.DynamoDB.DocumentClient(options);
const client = new AWS.DynamoDB(options);

function createTable(params, callback) {
    client.createTable(params, callback);
}

function put(params, callback) {
    documentClient.put(params, callback);
}

function deleteItem(params, callback) {
    documentClient.delete(params, callback);
}

function get(params, callback) {
    documentClient.get(params, (error, data) => {
        let result = data;
        if (data != null && data.Item != null) {
            result = data.Item;
        }
        if (callback != null) {
            callback(error, result);
        }
    });
}

module.exports = {
    createTable: createTable,
    put: put,
    get: get,
    delete: deleteItem,
};