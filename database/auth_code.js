'use strict';
const dynamodb = require('./dynamodb');
const config = require('../config');

function createDb(callback) {
    const params = {
        TableName: config.database.AuthCode,
        KeySchema: [
            { AttributeName: 'authCode', KeyType: 'HASH' },
            { AttributeName: 'appId', KeyType: 'RANGE' },
        ],
        AttributeDefinitions: [
            { AttributeName: 'authCode', AttributeType: 'S' },
            { AttributeName: 'appId', AttributeType: 'S' },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 2,
            WriteCapacityUnits: 2,
        },
    };
    dynamodb.createTable(params, callback);
}

createDb((error, data) => {
    console.log('===error===', error);
});