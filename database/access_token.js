'use strict';
const dynamodb = require('./dynamodb');
const config = require('../config');

function createDb(callback) {
    const params = {
        TableName: config.database.AccessToken,
        KeySchema: [
            { AttributeName: 'accessToken', KeyType: 'HASH' },
        ],
        AttributeDefinitions: [
            { AttributeName: 'accessToken', AttributeType: 'S' },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 2,
            WriteCapacityUnits: 2,
        },
    };
    dynamodb.createTable(params, callback);
}

createDb((error, data) => {
    console.log('===error===', error);
});