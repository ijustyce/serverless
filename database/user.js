'use strict';
const dynamodb = require('./dynamodb');
const config = require('../config');

function createDb(callback) {
    const params = {
        TableName: config.database.User,
        KeySchema: [
            { AttributeName: 'userName', KeyType: 'HASH' },
        ],
        AttributeDefinitions: [
            { AttributeName: 'userName', AttributeType: 'S' },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 2,
            WriteCapacityUnits: 2,
        },
    };
    dynamodb.createTable(params, callback);
}

createDb((error, data) => {
    console.log('===error===', error);
});