'use strict';
const dynamodb = require('./dynamodb');
const config = require('../config');

function createDb(callback) {
    const params = {
        TableName: config.database.VerifyCode,
        KeySchema: [
            { AttributeName: 'phone', KeyType: 'HASH' },
            { AttributeName: 'type', KeyType: 'RANGE' },
        ],
        AttributeDefinitions: [
            { AttributeName: 'phone', AttributeType: 'S' },
            { AttributeName: 'type', AttributeType: 'S' },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 2,
            WriteCapacityUnits: 2,
        },
    };
    dynamodb.createTable(params, callback);
}

createDb((error, data) => {
    console.log('===error===', error);
});