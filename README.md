#Auth 2 server

1、/code  
这个接口首先调用：user/login.authorizationCode，然后调用 user/model.getClient，这里返回了refresh token  接着调用 user/login.options.authenticateHandler 这个方法里判断用户登录信息并返回用户名称，你应该在这里连接数据库并校验
接着将调用到 user/model.saveAuthorizationCode 你应该在这里存储生成的code。然后调用到 user/login.authorizeHandler 里的then 这里不需要修改！  
  
2、/auth
当使用token作为凭证时，先调用 user/model 里的 getAccessToken 这里你需要校验token 并返回

3、/token  
先调用 user/model 里的getClient 接着 getRefreshToken 接着调用 revokeToken （删除旧的token），接着调用saveToken，然后回到 user/login  
  
还需要考虑：1、token的过期、刷新。可以使用token作为凭证进行验证。  
  
## 验证流程：  
1、用户输入密码时，调用 /code 获取到 autorization_code  
2、通过autorization_code 调用 /token 获得 refres_token 以及 access_token  
3、通过access_token 调用 /auth 接口 auth 接口 则调用 getAccessToken 获取用户信息！  

## 参考：  
1、https://oauth2-server.readthedocs.io/en/latest/docs/getting-started.html  
2、https://developers.douban.com/wiki/?title=oauth2  

## 数据库设计
1、Auth_code code 主键、userId  
2、Access_token accessToken 主键，userId  
3、Refresh_token refreshToken 主键， userId  
5、User_info   userName、password userName 主键

## 接口文档：
1、/newUser  put  
参数放在body 里：  
```json
{
    "userName": "chun14",
    "password": "Hello World"
}
```  
返回：
```json
{
    "code": 100,
    "message": "Success",
    "data": {
        "appId": "client_id",
        "accessToken": "d290ebc3ad0ddb102c1cc4f8b2b72d19a236a94c",
        "refreshToken": "f009e2f9d2cbfa9cd88ffb1e990eeb5b344e975c"
    }
}
```
2、/code   get  
参数：password、userName  
返回：
```json
{
    "code": 100,
    "message": "操作成功",
    "data": {
        "user": {
            "userName": "chun1"
        },
        "authorizationCode": "72675fce3bf4b36cbd014c4cee9248b6e1ab7bbd"
    }
}
```
3、/token  get
参数：authorizationCode  值为上文里返回的 authorizationCode  
返回：  
```json
{
    "code": 100,
    "message": "操作成功",
    "data": {
        "appId": "client_id",
        "accessToken": "c37c18e9a5afb68877ce36d82316202eb6e1457c",
        "refreshToken": "07c906faf879aae9eeff84b9fc6cc78206183045"
    }
}
```  
4、/login  /code 和  /token 合二为一的接口，get，参数和/code 一致，返回和 /token 一致  
注意，该接口针对同一个人不能频繁调用，dynamodb 和 nodejs的异步性会导致出现问题。
