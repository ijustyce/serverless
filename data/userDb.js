'use strict';
const dynamodb = require('../database/dynamodb');
const config = require('../config');
const security = require('../utils/security');

function createUser(userName, password, callback) {
    const params = {
        TableName: config.database.User,
        Item: {
            'userName': userName,
            'password': security.md5(password, userName),
        },
        ConditionExpression: 'attribute_not_exists (userName)',
    };
    dynamodb.put(params, (error, data) => {
        if (error != null && error.code == 'ConditionalCheckFailedException') {
            callback('userName exists...', null);
        } else {
            callback(error, data);
        }
    });
}

function saveUser(userInfo, callback) {
    const params = {
        TableName: config.database.User,
        Item: userInfo,
    };
    dynamodb.put(params, callback);
}

function getByNameAndPw(userName, password, callback) {
    getByUserName(userName, (error, data) => {
        if (data != null && data.password != null) {
            if (password == data.password) {
                callback(error, data);
                return;
            }
        }
        callback(error, null);
    });
}

function getByUserName(userName, callback) {
    const params = {
        TableName: config.database.User,
        Key: {
            'userName': userName,
        },
    };
    dynamodb.get(params, callback);
}

function getRefreshToken(refreshToken, callback) {
    const params = {
        TableName: config.database.RefreshToken,
        Key: {
            'refreshToken': refreshToken,
            'appId': appId,
        },
    };
    dynamodb.get(params, callback);
}

function getAccessToken(accessToken, callback) {
    const params = {
        TableName: config.database.AccessToken,
        Key: {
            'accessToken': accessToken,
        },
    };
    dynamodb.get(params, callback);
}

function getAuthCode(authCode, appId, callback) {
    const params = {
        TableName: config.database.AuthCode,
        Key: {
            'authCode': authCode,
            'appId': appId,
        },
    };
    dynamodb.get(params, callback);
}

function saveRefreshToken(userName, refreshToken, appId, callback) {
    const params = {
        TableName: config.database.RefreshToken,
        Item: {
            'userName': userName,
            'refreshToken': refreshToken,
            'appId': appId,
            'date': Date.now(),
        },
    };
    dynamodb.put(params, (error, data) => {
        console.log('===saveRefreshToken===', error);
        if (callback != null) {
            callback(error, data);
        }
    });
}

function saveAccessToken(userName, accessToken, appId, callback) {
    const params = {
        TableName: config.database.AccessToken,
        Item: {
            'userName': userName,
            'accessToken': accessToken,
            'appId': appId,
            'date': Date.now(),
        },
    };
    dynamodb.put(params, (error, data) => {
        console.log('===saveAccessToken===', error);
        if (callback != null) {
            callback(error, data);
        }
    });
}

function saveAuthCode(userName, authCode, appId, callback) {
    const params = {
        TableName: config.database.AuthCode,
        Item: {
            'userName': userName,
            'authCode': authCode,
            'appId': appId,
            'date': Date.now(),
        },
    };
    dynamodb.put(params, (error, data) => {
        console.log('===saveAuthCode===', error);
        if (callback != null) {
            callback(error, data);
        }
    });
}

function deleteRefreshToken(refreshToken, callback) {
    const params = {
        TableName: config.database.RefreshToken,
        Item: {
            'refreshToken': refreshToken,
        },
    };
    dynamodb.delete(params, callback);
}

function deleteAccessToken(accessToken, callback) {
    const params = {
        TableName: config.database.AccessToken,
        Item: {
            'accessToken': accessToken,
        },
    };
    dynamodb.delete(params, callback);
}

function deleteAuthCode(authCode, callback) {
    const params = {
        TableName: config.database.AuthCode,
        Item: {
            'authCode': authCode,
        },
    };
    dynamodb.delete(params, callback);
}

module.exports = {
    createUser: createUser,
    getByNameAndPw: getByNameAndPw,
    getByUserName: getByUserName,
    getRefreshToken: getRefreshToken,
    getAccessToken: getAccessToken,
    saveRefreshToken: saveRefreshToken,
    saveAccessToken: saveAccessToken,
    saveAuthCode: saveAuthCode,
    getAuthCode: getAuthCode,
    saveUser: saveUser,
    deleteRefreshToken: deleteRefreshToken,
    deleteAccessToken: deleteAccessToken,
    deleteAuthCode: deleteAuthCode,
};

// createUser('chun', 'chun', (error, data) => {
//     console.log('===error===', error);
//     console.log('===data===', data);
// });