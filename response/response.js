'use strict';

function getJsonByLang(lang) {
    let json;
    switch (lang.toLowerCase()) {
        case 'en':
            json = require('./en/response.json');
            break;
        case 'zh-rcn':
            json = require('./cn/response.json');
            break;
        default:
            json = require('./cn/response.json');
            break;
    }
    return json;
}

function getLang(event) {
    const header = event.headers;
    if (header == null) {
        return 'zh-rCN';
    }
    let lang = header.Language;
    if (lang == null) {
        lang = 'zh-rCN';
    }
    return lang;
}

function get(event) {
    return getJsonByLang(getLang(event));
}

function response(callback, event, message, data) {
    let result = message;
    if (message == null || message.statusCode == null) {
        result = get(event).Success;
    }
    if (data != null) {
        result.data = data;
    }
    const responseValue = {
        statusCode: 200,
        body: JSON.stringify(result),
    };
    callback(null, responseValue);
}

module.exports = {
    get: get,
    response: response,
};