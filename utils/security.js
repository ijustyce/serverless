'use strict';
const uuidUtils = require('uuid');
const crypto = require('crypto');

const maxRandomNum = 10;

function md5(value, salt) {
    const saltPassword = value + ':' + salt;
    const md5 = crypto.createHash('md5');
    return md5.update(saltPassword).digest('hex');
}

function uuid() {
    return uuidUtils.v1();
}

function verifyCode(len) {
    let result = '';
    for (let index = 0; index < len; index++) {
        result += Math.floor(Math.random() * maxRandomNum);
    }
    return result;
}

module.exports = {
    md5: md5,
    verifyCode: verifyCode,
    uuid: uuid,
};