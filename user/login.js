'use strict';

const userDb = require('../data/userDb');
const verifyCode = require('../data/verifyCodeDb');
const response = require('../response/response');
const auth = require('./auth');
const security = require('../utils/security');

const codeLifeTime = 300000;
const verifyCodeLen = 6;
const findPwAction = '#ssyer.com#findPw#';
const codeLoginAction = '#ssyer.com#codeLogin#';

function phoneCodeLogin(event, context, callback) {
    const data = event.queryStringParameters;
    const phone = data.phone;
    const code = data.code;
    verifyCode.getLoginCode(phone, code, (error, data) => {
        const errorMsg = checkCodeError(data, event);
        if (errorMsg != null) {
            response.response(callback, event, errorMsg, null);
            return;
        }
        if (security.md5(data.code, codeLoginAction) != code) {
            response.response(callback, event, response.get(event).ErrorCode, null);
            return;
        }
        userDb.getByUserName(phone, (error, userInfo) => {
            if (userInfo == null || userInfo.userName == null || userInfo.password == null) {
                auth.createUserAndLogin(phone, phone, event, context, callback);
                return;
            }
            auth.internalLogin(event, userInfo.userName, userInfo.password, context, callback);
        });
    });
}

function resetPw(event, context, callback) {
    const body = event.body;
    let data;
    try {
        data = JSON.parse(body);
    } catch (e) {
        response.response(callback, event, response.get(event).RuntimeException, null);
        return;
    }
    const phone = data.phone;
    const code = data.code;
    verifyCode.getFindPwCode(phone, code, (error, data) => {
        const errorMsg = checkCodeError(data, event);
        if (errorMsg != null) {
            response.response(callback, event, errorMsg, null);
            return;
        }
        if (security.md5(data.code, findPwAction) != code) {
            response.response(callback, event, response.get(event).ErrorCode, null);
            return;
        }
        userDb.getByUserName(phone, (error, userInfo) => {
            if (userInfo == null || userInfo.userName == null || userInfo.password == null) {
                response.response(callback, event, response.get(event).RuntimeException, null);
                return;
            }
            userInfo.password = security.md5(data.newPassword, userInfo.userName);
            userDb.saveUser(userInfo);
            response.response(callback, event, null, null);
        });
    });
}

function checkCodeError(data, event) {
    if (data == null || data.date == null) {
        return response.get(event).ErrorCode;
    }
    if (date.date < Date.now() - codeLifeTime) {
        return response.get(event).CodeTimeOut;
    }
    return null;
}

function getLoginCode(event, context, callback) {
    const data = event.queryStringParameters;
    const phone = data.phone;
    const action = data.action;
    if (security.md5(phone, codeLoginAction) != action) {
        response.response(callback, event, response.get(event).ErrorAction, null);
    }
    verifyCode.getLoginCode(phone, code, (error, data) => {
        let code;
        if (data != null && checkCodeError(data.code, data) == null) {
            code = data.code;
        } else {
            code = security.verifyCode(verifyCodeLen);
        }
        verifyCode.saveLoginCode(phone, code);
        //  TODO send sms
        response.response(callback, event, null, null);
    });
}

function getFindPwCode(event, context, callback) {
    const data = event.queryStringParameters;
    const phone = data.phone;
    const action = data.action;
    if (security.md5(phone, findPwAction) != action) {
        response.response(callback, event, response.get(event).ErrorAction, null);
    }
    verifyCode.getFindPwCode(phone, code, (error, data) => {
        let code;
        if (data != null && checkCodeError(data.code, data) == null) {
            code = data.code;
        } else {
            code = security.verifyCode(verifyCodeLen);
        }
        verifyCode.saveFindPwCode(phone, code);
        //  TODO send sms
        response.response(callback, event, null, null);
    });
}

module.exports = {
    phoneCodeLogin: phoneCodeLogin,
    getLoginCode: getLoginCode,
    getFindPwCode: getFindPwCode,
    resetPw: resetPw,
};