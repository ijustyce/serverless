'use strict';
const userDb = require('../data/userDb');

/**
 *  for more information please vist https://oauth2-server.readthedocs.io/en/latest/model/spec.html
 */

/**
 * @return {date} current date + 2minutes
 */
function getDate() {
    const date = new Date();
    const addMinutes = 2;
    date.setMinutes(date.getMinutes() + addMinutes);
    console.log(date.toLocaleString());
    return date;
}

// module.exports.generateAccessToken = (client, user, scope, callback) => {
// 	console.log("===generateAccessToken===")
// }

// module.exports.generateRefreshToken = (client, user, scope, callback) => {
// 	console.log("===generateRefreshToken===")
// }

// module.exports.generateAuthorizationCode = (client, user, scope, callback) => {
// 	console.log("===generateAuthorizationCode===")
// 	console.log("===client===", client)
// 	console.log("===user===", user)
// 	console.log("===scope===", scope)
// 	callback(null, "code_for_login")
// }

module.exports.getAccessToken = (accessToken, callback) => {
    console.log('===getAccessToken===');
    console.log('===accessToken===', accessToken);
    userDb.getAccessToken(accessToken, (error, data) => {
        callback(null, {
            accessToken: accessToken,
            accessTokenExpiresAt: getDate(),
            date: data.date,
            client: {
                id: data.appId,
            },
            user: { 'userName': data.userName },
        });
    });
};

module.exports.getRefreshToken = (refreshToken, callback) => {
    console.log('===getRefreshToken===');
    console.log('===refreshToken===', refreshToken);
    callback(null, {
        refreshToken: 'accessToken',
        refreshTokenExpiresAt: getDate(),
        scope: 'all',
        client: {
            id: 'clientId',
        },
        user: {
            name: 'chun',
        },
    });
};

module.exports.getAuthorizationCode = (authorizationCode, callback) => {
    console.log('===getAuthorizationCode===');
};

module.exports.getClient = (clientId, clientSecret, callback) => {
    console.log('===getClient===');
    callback(null, {
        'id': 'clientId',
        'redirectUris': 'https://www.ssyer.com',
        'grants': ['authorization_code', 'password', 'refresh_token', 'client_credentials'],
    });
};

module.exports.getUser = (username, password, callback) => {
    console.log('===getUser===');
};

module.exports.getUserFromClient = (client, callback) => {
    console.log('===getUserFromClient===');
};

module.exports.saveToken = (token, client, user, callback) => {
    console.log('===saveToken===');
    console.log('===token===', token);
    token.client = client;
    token.user = user;
    callback(null, token);
};

module.exports.saveAuthorizationCode = (code, client, user, callback) => {
    console.log('===saveAuthorizationCode===');
    code.client = client;
    code.client.id = 'client_id';
    code.user = user;
    userDb.saveAuthCode(user.userName, code.authorizationCode, code.client.id, null);
    callback(null, code);
};

module.exports.revokeToken = (token, callback) => {
    console.log('===revokeToken===');
    console.log('===token===', token);
    callback(null, true); //	delete old token if success return true or false
};

module.exports.revokeAuthorizationCode = (code, callback) => {
    console.log('===revokeAuthorizationCode===');
};

module.exports.validateScope = (user, client, scope, callback) => {
    console.log('===validateScope===');
};

module.exports.verifyScope = (accessToken, scope, callback) => {
    console.log('===verifyScope===');
};